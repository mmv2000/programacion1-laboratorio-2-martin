#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

#rellena la lista bidimensional
def rellenar(n):
	L = []
	for i in range(n):
		L.append([])
		for j in range(n):
			L[i].append(random.randint(0, 20))
	print(L)
	return L

#invierte las filas
def invertir(lista, n):
	for i in range(n - 1, -1 , -1):
		for j in range(n):
			print(lista[i][j], end='')
		print("\n")

n = int(input("Ingrese el tamaño del arreglo:"))
lista = rellenar(n)
invertir(lista, n)
