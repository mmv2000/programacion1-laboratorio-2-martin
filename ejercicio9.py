#!/usr/bin/env python
# -*- coding: utf-8 -*-

N = 6
A = []
i = 0
j = 0

#genera la matriz identidad
for i in range(N):
	fila = []
	for j in range(N):
		if i==j:
			casilla = 1
		else:
			casilla = 0
		fila.append(casilla)
	A.append(fila)
print(A)
